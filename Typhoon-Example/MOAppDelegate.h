//
//  MOAppDelegate.h
//  Typhoon-Example
//
//  Created by Maciej Oczko on 10/14/13.
//  Copyright (c) 2013 Polidea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
