//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOAccountManager.h"
#import "MOApiClientProtocol.h"
#import "MOUserManager.h"
#import "Defines.h"


@interface MOAccountManager ()
@property(nonatomic, strong) id <MOApiClientProtocol> apiClient;
@property(nonatomic, strong) MOUserManager *userManager;
@end

@implementation MOAccountManager {

}

- (id)init {
    self = [super init];
    if (self) {
        assert(_apiClient == nil);
    }

    return self;
}

@end
