//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>


@interface MOActionHandler : NSObject
@property(nonatomic, readonly) NSString *name;
- (instancetype)initWithName:(NSString *)name;

@end
