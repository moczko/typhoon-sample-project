//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOActionHandler.h"
#import "Defines.h"


@implementation MOActionHandler {

}

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }

    return self;
}

@end
