//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>
#import "MOApiClientProtocol.h"


@interface MOApiClient : NSObject <MOApiClientProtocol>
@end
