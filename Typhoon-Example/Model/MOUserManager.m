//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOUserManager.h"
#import "MOCoreDataManager.h"
#import "MOKeyChainAccessor.h"
#import "TyphoonAutowire.h"
#import "TyphoonIntrospectionUtils.h"
#import "Defines.h"


@interface MOUserManager ()
@property(nonatomic, strong) MOCoreDataManager *coreDataManager;
@property(nonatomic, strong) MOKeyChainAccessor *keyChainAccessor;
@end


@implementation MOUserManager {

}

typhoon_autoWire(@selector(coreDataManager), @selector(keyChainAccessor))

- (void)awakeAfterCreation {
    assert(_coreDataManager);
    assert(_keyChainAccessor);

    HELP_LOG
}

@end
