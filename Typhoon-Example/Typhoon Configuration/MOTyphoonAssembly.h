//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>
#import "TyphoonAssembly.h"


@interface MOTyphoonAssembly : TyphoonAssembly
- (id)rootViewController;
- (id)modalViewController;
- (id)accountManager;
- (id)userManager;
- (id)coreDataManager;
- (id)apiClient;
@end
