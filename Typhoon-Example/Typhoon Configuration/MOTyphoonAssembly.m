//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOTyphoonAssembly.h"
#import "TyphoonDefinition.h"
#import "MOAccountManager.h"
#import "TyphoonInitializer.h"
#import "MOApiClient.h"
#import "MOUserManager.h"
#import "MOKeyChainAccessor.h"
#import "MOCoreDataManager.h"
#import "MOModalViewController.h"
#import "MORootViewController.h"


@implementation MOTyphoonAssembly {

}

- (id)rootViewController {
    return [TyphoonDefinition withClass:[MORootViewController class] initialization:^(TyphoonInitializer *initializer) {
        initializer.selector = @selector(initWithNibName:bundle:);
    }];
}

- (id)modalViewController {
    return [TyphoonDefinition withClass:[MOModalViewController class] initialization:^(TyphoonInitializer *initializer) {
        initializer.selector = @selector(initWithAccountManager:);
        [initializer injectWithDefinition:[self accountManager]];
    }];
}

- (id)accountManager {
    return [TyphoonDefinition withClass:[MOAccountManager class] initialization:^(TyphoonInitializer *initializer) {
        initializer.selector = @selector(init);
    }                        properties:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(apiClient) withDefinition:[self apiClient]];
        [definition injectProperty:@selector(userManager) withDefinition:[self userManager]];
    }];
}

- (id)userManager {
    return [TyphoonDefinition withClass:[MOUserManager class] properties:^(TyphoonDefinition *definition) {
        [definition setAfterPropertyInjection:@selector(awakeAfterCreation)];
    }];
}

- (id)coreDataManager {
    return [TyphoonDefinition withClass:[MOCoreDataManager class] properties:^(TyphoonDefinition *definition) {
        [definition setAfterPropertyInjection:@selector(awakeAfterCreation)];
        [definition setScope:TyphoonScopeSingleton];
        [definition setLazy:NO];
    }];
}

- (id)apiClient {
    return [TyphoonDefinition withClass:[MOApiClient class]];
}

- (id)keyChainAccessor {
    return [TyphoonDefinition withClass:[MOKeyChainAccessor class]];
}

@end
