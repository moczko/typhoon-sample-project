//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOModalView.h"
#import "MOModalViewDelegate.h"


@implementation MOModalView {

}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor magentaColor];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [button setTitle:@"Hide modal" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(didTapHideModalButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[button]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)]];
    }

    return self;
}

- (void)didTapHideModalButton:(UIButton *)button {
    [self.delegate hideModalView];
}

@end
