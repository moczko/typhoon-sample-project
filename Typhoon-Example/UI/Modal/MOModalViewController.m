//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MOModalViewController.h"
#import "MOModalView.h"
#import "MOAccountManager.h"


@implementation MOModalViewController {

}


- (instancetype)initWithAccountManager:(MOAccountManager *)accountManager {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _accountManager = accountManager;
    }

    return self;
}

- (void)loadView {
    MOModalView *view = [[MOModalView alloc] initWithFrame:CGRectZero];
    view.delegate = self;
    self.view = view;
}

#pragma mark -

- (void)hideModalView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
