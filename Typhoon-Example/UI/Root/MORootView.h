//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>

@protocol MORootViewDelegate;


@interface MORootView : UIView
@property(nonatomic, weak) id <MORootViewDelegate> delegate;
@end
