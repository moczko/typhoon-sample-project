//
//  Created by Maciej Oczko on 10/15/13.
//


#import "MORootView.h"
#import "MORootViewDelegate.h"


@implementation MORootView {

}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [button setTitle:@"Open modal" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(didTapOpenModalButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[button]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)]];
    }

    return self;
}

- (void)didTapOpenModalButton:(UIButton *)button {
    [self.delegate openModalView];
}

@end
