//
//  Created by Maciej Oczko on 10/14/13.
//


#import <Foundation/Foundation.h>
#import "MORootViewDelegate.h"


@interface MORootViewController : UIViewController <MORootViewDelegate>
@end
