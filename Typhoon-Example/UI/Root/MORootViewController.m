//
//  Created by Maciej Oczko on 10/14/13.
//


#import "MORootViewController.h"
#import "MORootView.h"
#import "MOModalViewController.h"
#import "MOTyphoonAssembly.h"
#import "TyphoonComponentFactory.h"

@implementation MORootViewController {

}


- (void)loadView {
    MORootView *view = [[MORootView alloc] initWithFrame:CGRectZero];
    view.delegate = self;
    self.view = view;
}

#pragma mark -

- (void)openModalView {

    TyphoonComponentFactory *factory = [TyphoonComponentFactory defaultFactory];
    MOModalViewController *controller = [factory componentForType:[MOModalViewController class]];
    [self presentViewController:controller animated:YES completion:nil];
}

@end
