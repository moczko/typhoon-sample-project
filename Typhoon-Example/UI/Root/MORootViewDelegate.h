//
//  Created by Maciej Oczko on 10/15/13.
//


#import <Foundation/Foundation.h>

@protocol MORootViewDelegate <NSObject>
- (void)openModalView;
@end
